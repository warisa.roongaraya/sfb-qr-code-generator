import sys
import os


def get_token_file_path():
    """
    The function `get_token_file_path` returns the file path of the `api_token.txt` file, taking into
    account whether the code is running in bundled mode or development mode.
    :return: the file path of the "api_token.txt" file.
    """
    file_name = "api_token.txt"
    if getattr(sys, 'frozen', False):
        # Running in bundled mode
        base_dir = os.path.dirname(sys.executable)
    else:
        # Running in development mode
        script_dir = os.path.dirname(os.path.abspath(__file__))
        base_dir = os.path.dirname(script_dir)  # Go up one level
    return os.path.join(base_dir, file_name)
