import tkinter as tk
import customtkinter
from api import create_qr_code, download_metadata, randomize_id, initialize_coscine_client
from tkinter import messagebox
from print import PRINTER_NAME, print_square_image

customtkinter.set_appearance_mode("light")
customtkinter.set_default_color_theme("blue")


class ScrollableEntryFrame(customtkinter.CTkScrollableFrame):
    def __init__(self, master, width=None, height=None, project=None, qr_code_id=None, display_name=None,
                 is_test_series=None, **kwargs):
        """
        This function initializes a custom tkinter frame with input fields based on metadata and adds
        them to the frame.
        
        :param master: The `master` parameter is the parent widget or the root window where this widget
        will be placed
        :param width: The `width` parameter is used to set the width of the widget. It determines the
        number of characters that can be displayed horizontally in the widget
        :param height: The `height` parameter is used to specify the height of the widget. It determines
        how many rows or lines the widget will occupy vertically
        :param project: The "project" parameter is an object that represents a project. It is used to
        access resources and metadata related to the project
        :param qr_code_id: The `qr_code_id` parameter is used to identify a specific QR code. It is used
        to retrieve the resource associated with the QR code and its metadata
        """

        super().__init__(master, **kwargs)
        if qr_code_id == None or project == None:
            messagebox.showerror("Error", "No input provided!")

        self.display_name = display_name
        self.project = project
        self.resource = project.resource(display_name)
        self.metadata = self.resource.metadata_form()
        self.qr_code_id = qr_code_id
        self.display_name = display_name
        # If this is a test series, use same display name but different full name
        # TODO: Check if this is possible
        self.is_test_series = is_test_series

        def add_item(self, key):
            """
            The add_item function adds a key to a data structure.
            
            :param self:
            :param key: The "key" parameter is the value that you want to add to the data structure
            """
            entry_frame = customtkinter.CTkFrame(self)
            entry_frame.pack(fill="x", pady=(0, 10))
            entry_frame.item_name = key

            label_text = key
            if self.metadata.field(key).is_required:
                label_text = key + "*"
            label_name = customtkinter.CTkLabel(entry_frame, text=label_text, wraplength=67)
            label_name.pack(side="left")

            label_name.update()
            label_width = label_name.winfo_width()

            if self.metadata.field(key).is_controlled:
                vocabulary = self.metadata.field(key).vocabulary
                combobox = customtkinter.CTkComboBox(entry_frame, values=vocabulary.keys())
                combobox.set("------")
                combobox.pack(side="left", padx=(75 - label_width, 0))

            elif (self.metadata.field(key).datatype == bool):
                combobox = customtkinter.CTkComboBox(entry_frame, values=["Yes", "No"])
                combobox.set("------")
                combobox.pack(side="left", padx=(75 - label_width, 0))

            else:
                entry = customtkinter.CTkEntry(entry_frame)
                entry.pack(side="left", padx=(75 - label_width, 0))

            self.entry_list.append(entry_frame)

            if self.width is not None:
                self.configure(width=self.width)
            if self.height is not None:
                self.configure(height=self.height)

        self.entry_list = []
        self.width = width
        self.height = height
        self.configure(width=width, height=height)
        for key in self.metadata:
            add_item(self, key)

    def generate_qr_code(self, selected_options, size_entry, time_of_printing_entry, popup_window1, popup_window2):
        """
        The function `create_qr_code_with_options` allows the user to select options for a QR code,
        specify its size, and print it a specified number of times.
        
        :param selected_options: The `selected_options` parameter in the `generate_qr_code` method is a
        list containing the options selected by the user for the QR code. These options are extracted
        from the checkboxes that the user has selected. Each option is a string representing a specific
        attribute or feature of the QR code
        :param size_entry: The `size_entry` parameter in the `generate_qr_code` method is a tkinter
        Entry widget where the user can input the size of the QR code in centimeters. The method
        retrieves the value entered in this entry widget to determine the size of the QR code to be
        generated
        :param time_of_printing_entry: The `time_of_printing_entry` parameter in the `generate_qr_code`
        method is a tkinter Entry widget where the user can input the number of times they want to print
        the QR code. The value entered in this entry field is retrieved using the `get()` method to
        determine how many times
        :param popup_window1: The `popup_window1` parameter in the `generate_qr_code` method is a
        reference to a pop-up window that is already open. It is used to close this pop-up window after
        the QR code generation process is completed. This parameter allows the method to interact with
        the pop-up window and
        :param popup_window2: The `popup_window2` parameter in the `generate_qr_code` method is a
        reference to a pop-up window that is used to display options for printing the QR code. It is
        passed as an argument when calling the `generate_qr_code` method to ensure that the pop-up
        window is
        :return: The `generate_qr_code` function returns nothing explicitly. It performs various
        operations such as creating a QR code, uploading it, printing it, handling exceptions, and
        displaying success or error messages using `messagebox`. The function also clears entry fields
        and closes popup windows as needed.
        """

        size_str = size_entry.get()
        print_n_times = int(time_of_printing_entry.get())
        try:
            qr_code_size = float(size_str)
            top_metadata = []
            for key in selected_options:
                metadata_value = f"{key} = {self.metadata.field(key).values}"
                top_metadata.append(metadata_value)
            file_name, file_path = create_qr_code(resource=self.resource, size_cm=qr_code_size,
                                                  qr_code_id=self.qr_code_id, top_metadata=top_metadata)

            try:
                self.resource.upload(file_name, file_path, self.metadata)
                messagebox.showinfo("Success", f"QR code created successfully. QR code id: {self.qr_code_id}")
                download_metadata(qrcode_id=self.qr_code_id, metadata_form=self.metadata)

                for entry_frame in self.entry_list:
                    # For test series, don't clear previous entries
                    if not self.is_test_series:
                        # Reset entry and combobox values to defaults
                        entry = entry_frame.winfo_children()[1]
                        if isinstance(entry, customtkinter.CTkComboBox):
                            entry.set("------")
                        else:
                            entry.delete(0, "end")
                for i in range(print_n_times):
                    try:
                        print_square_image(qr_code_id=self.qr_code_id, size_cm=qr_code_size)
                        messagebox.showinfo("Success",
                                            f"Printed a square image {qr_code_size} cm x {qr_code_size} cm to {PRINTER_NAME}.")
                    except Exception as e:
                        messagebox.showerror("Error", f"Error printing to {PRINTER_NAME}: {str(e)}")

                # Close the popup window
                popup_window1.destroy()
                popup_window2.destroy()

                # Change the id of the qr code for the next sample in the series
                if self.is_test_series:
                    user_continue = messagebox.askyesno("Continue recording samples?",
                                                        "Do you want to continue creating samples?")
                    if user_continue:
                        self.qr_code_id = randomize_id()
                        client = initialize_coscine_client()
                        previous_metadata_scheme = self.resource.application_profile
                        # Use new resource name but same display name so that user is aware that this is the same
                        # test series
                        self.resource = self.project.create_resource(
                            self.qr_code_id,  #Resource name
                            self.display_name,  #Display name
                            "Probe",
                            client.license("The Unlicense"),  #TODO: Ask Kevin about this
                            client.visibility("Project Members"),
                            [client.discipline("Production Technology 401")],
                            client.resource_type("rdsrwth"),
                            1,  # Quota
                            previous_metadata_scheme  # A application profile
                        )
            except Exception as e:
                messagebox.showerror("Upload Error", f"An error occurred during upload: {str(e)}")
                print(self.metadata)
                print(e)
                return

        except ValueError:
            messagebox.showerror("Error", "Please enter a valid number for QR code size.")
            size_entry.delete(0, "end")  # Clear the entry field
            return

    def create_qr_code_with_options(self, options, checkbox_vars, pop_up_window):
        """
        This function creates a QR code with selected options and allows the user to specify the size of
        the QR code.
        
        :param options: The `options` parameter is a list of available options for the QR code. Each
        option is a string
        :param checkbox_vars: The `checkbox_vars` parameter is a list of variables that are associated
        with checkboxes. These variables are used to determine whether a checkbox is selected or not
        :param pop_up_window: The `pop_up_window` parameter is a reference to a pop-up window that is
        already open. It is used as an argument when calling the `generate_qr_code` method
        """

        selected_options = [options[i] for i, var in enumerate(checkbox_vars) if var.get()]
        if len(selected_options) > 5:
            messagebox.showerror("Error", "Please select a maximum of 5 options.")
        else:
            qr_code_print_option_popup_window = tk.Toplevel(self)
            qr_code_print_option_popup_window.title("Insert the size of the QR Code")
            qr_code_print_option_popup_window.geometry("200x200")
            qr_code_print_option_popup_window.resizable(False, False)

            size_label = customtkinter.CTkLabel(qr_code_print_option_popup_window,
                                                text="Enter QR Code Size (Centimeter):")
            size_label.pack()

            size_entry = customtkinter.CTkEntry(qr_code_print_option_popup_window)
            size_entry.pack()

            time_of_printing_label = customtkinter.CTkLabel(qr_code_print_option_popup_window,
                                                            text="Print the QR Code(times):")
            time_of_printing_label.pack()

            time_of_printing_entry = customtkinter.CTkEntry(qr_code_print_option_popup_window)
            # Set the default value to 1
            time_of_printing_entry.insert(0, "1")
            time_of_printing_entry.pack()

            generate_button = customtkinter.CTkButton(qr_code_print_option_popup_window, text="Generate",
                                                      command=lambda:
                                                      self.generate_qr_code(selected_options, size_entry,
                                                                            time_of_printing_entry,
                                                                            popup_window1=pop_up_window,
                                                                            popup_window2=qr_code_print_option_popup_window))
            generate_button.pack(pady=10)

    def record_values(self):
        """
        The `record_values` function records values from entry fields, checks for required inputs, and
        creates checkboxes for selecting priority metadata.
        """
        for entry_frame in self.entry_list:
            entry = entry_frame.winfo_children()[1]  # Assuming the entry is always the second child
            key = entry_frame.item_name

            if isinstance(entry, customtkinter.CTkComboBox):
                if entry.get() == "------" and self.metadata.field(key).is_required:
                    messagebox.showerror("Error", f"Input needed for {key}!")
                else:
                    value = entry.get()
                    if self.metadata.field(key).datatype == bool:
                        if value == "Yes":
                            value = True
                        else:
                            value = False  #default value is False
            else:
                if entry.get() == "" and self.metadata.field(key).is_required:
                    messagebox.showerror("Error", f"Input needed for {key}!")
                else:
                    value = entry.get()

            if value != "" and value != "------":
                self.metadata[key] = value

        pop_up_window = tk.Toplevel(self)
        pop_up_window.title("Select priority metadata")
        pop_up_window.geometry("400x300")
        pop_up_window.resizable(False, False)

        scrollable_frame = customtkinter.CTkScrollableFrame(pop_up_window)
        scrollable_frame.pack(fill="both", expand=True)

        checkbox_vars = []
        options = self.metadata.keys()
        default_selected = options[:3]

        # Create and add checkboxes
        for option in options:
            var = tk.BooleanVar(value=option in default_selected)
            checkbox_vars.append(var)
            checkbox = customtkinter.CTkCheckBox(scrollable_frame, text=option, variable=var)
            checkbox.pack(anchor="w", padx=10, pady=5)
        record_button = customtkinter.CTkButton(pop_up_window, text="Create QR Code",
                                                command=lambda: self.create_qr_code_with_options(options, checkbox_vars,
                                                                                                 pop_up_window))
        record_button.pack()
