import os
import random
import string
import sys
import tkinter as tk
from datetime import date, datetime, time
from decimal import Decimal
from tkinter import messagebox
from tkinter import ttk
from typing import List, Tuple, Union

import coscine
import customtkinter as ctk
import qrcode

ctk.set_appearance_mode("light")
ctk.set_default_color_theme("blue")

DPI = 203


def get_token_file_path():
    """
    The function `get_token_file_path()` returns the file path of the `api_token.txt` file, taking into
    account whether the code is running in bundled mode or development mode.
    :return: the file path of the "api_token.txt" file.
    """
    file_name = "api_token.txt"
    if getattr(sys, 'frozen', False):
        # Running in bundled mode
        base_dir = os.path.dirname(sys.executable)
    else:
        # Running in development mode
        script_dir = os.path.dirname(os.path.abspath(__file__))
        base_dir = os.path.dirname(script_dir)  # Go up one level
    print(base_dir)
    return os.path.join(base_dir, file_name)


# The `SaveLocationPopup` class is a popup window that allows the user to select a project and save a
# location within that project.
class SaveLocationPopup:
    def __init__(self, parent, projects):
        """
        This function initializes a popup window for selecting a project and saving a file.
        
        :param parent: The `parent` parameter is the parent window or frame that the popup window will be
        attached to. It is usually passed as `self` when creating the popup window within a class
        :param projects: The "projects" parameter is a list of project objects. Each project object
        represents a project and has a "name" attribute. The list contains all the available projects
        that can be selected in the dropdown menu
        """
        self.parent = parent
        self.projects = projects
        self.selected_project = None
        self.isLocationSaved = False

        self.popup = tk.Toplevel(parent)
        self.popup.title("Choose Save Location")

        self.label = tk.Label(self.popup, text="Select a project:")
        self.label.pack(padx=10, pady=10)

        self.project_var = tk.StringVar()
        self.project_menu = ttk.Combobox(self.popup, textvariable=self.project_var,
                                         values=[project.name for project in projects])
        self.project_menu.pack(padx=10, pady=10)

        self.save_button = ctk.CTkButton(self.popup, text="Save", command=self.save)
        self.save_button.pack(padx=10, pady=10)

    def save(self):
        """
        The function saves the selected project and sets a flag indicating that the location has been
        saved.
        """
        self.selected_project = self.project_var.get()
        self.popup.destroy()
        self.isLocationSaved = True


def save_in_project_with_subprojects(root, top_level_projects):
    """
    The function saves a selected project and its subprojects in a project hierarchy.
    
    :param root: The root parameter is typically the main window or frame of the application. It is used
    to create the SaveLocationPopup and to wait for the user to make a selection before continuing
    :param top_level_projects: A list of top-level projects. Each project in the list may have
    subprojects
    :return: the last selected project.
    """
    selected_project = None
    popup = SaveLocationPopup(root, top_level_projects)
    root.wait_window(popup.popup)
    selected_project = popup.selected_project

    return selected_project


def initialize_coscine_client():
    """
    The function `initialize_coscine_client` initializes a client for the Coscine API by reading a token
    from a file and returning the client object.
    :return: The function `initialize_coscine_client()` returns an instance of the `coscine.ApiClient`
    class if the `api_token.txt` file is found and contains a valid token. Otherwise, it returns `None`.
    """
    token_file_path = get_token_file_path()
    print(token_file_path)
    if os.path.isfile(token_file_path):
        with open(token_file_path, 'r') as file:
            token = file.read().strip()
        client = coscine.ApiClient(token)
        # Set longer time out time than the default 60 s to prevent timeout and connection error
        client.timeout = 160
        return client
    else:
        print("api_token.txt not found.")
        return None


def randomize_id():
    """
    The function `randomize_id` generates a random ID string with a specific format.
    :return: The function `randomize_id` returns a randomly generated ID string in the format
    "SFB1120XXYYY", where XX are two random uppercase letters and YYY is a random 3-digit number between
    100 and 999.
    """
    random_letters = random.choices(string.ascii_uppercase, k=2)
    random_digits = random.randint(100, 999)
    id_str = "SFB1120" + ''.join(random_letters) + str(random_digits)
    return id_str


def generate_id(root):
    """
    The function `generate_id` generates a unique ID and saves it in a project with subprojects.
    
    :param root: The root window that the pop up window is going to pop up from
    :return: The `generate_id` function is returning a tuple containing the generated `id_str` and the
    `project` object.
    """
    while True:
        id_str = randomize_id()
        client = initialize_coscine_client()
        projects = client.projects(toplevel=False)
        project = save_in_project_with_subprojects(root, projects)
        try:
            project.resource(id_str, project.name)
            #project.resource(id_str)
        except:
            return id_str, project


def create_resource(root, metadata_scheme_uri, display_name):
    """
    The function creates a resource on the Coscine repository with the specified metadata scheme and
    returns the project and QR code ID.
    
    :param display_name:
    :param metadata_scheme_uri:
    :param root: The main window or frame of the application. It is needed as a parameter for generate_id()
    :param metadata_scheme: The metadata_scheme parameter is the Application Profile user chooses.
    :return: the project object and the qr_code_id.
    """
    client = initialize_coscine_client()
    qr_code_id, project_name = generate_id(root)
    project = client.project(project_name, toplevel=False)
    metadata_scheme = client.application_profile(metadata_scheme_uri)

    resource = project.create_resource(
        qr_code_id,  #Resource name
        display_name,  #Display name
        "Probe",
        client.license("The Unlicense"),  #TODO: Ask Kevin about this
        client.visibility("Project Members"),
        [client.discipline("Production Technology 401")],
        client.resource_type("rdsrwth"),
        1,  # Quota
        metadata_scheme  # A application profile
    )
    return project, qr_code_id


def create_qr_code(resource, size_cm, qr_code_id, top_metadata=[]):
    """
    The function `create_qr_code` generates a QR code image based on the provided resource, size, and
    metadata, and saves it as a PNG file.
    
    :param resource: Resource is a type of instance in Coscine, where all the information about your sample is stored.
    :param size_cm: The `size_cm` parameter represents the desired size of the QR code in centimeters
    :param qr_code_id: The `qr_code_id` parameter is a unique identifier for the QR code. It is used to
    generate a file name for the QR code image and to identify the QR code when it is saved
    :param top_metadata: The `top_metadata` parameter is a list of additional metadata that you want to
    include in the QR code. Each metadata item in the list will be appended to the `qr_data` string
    before generating the QR code. The metadata items should be strings
    :return: The function `create_qr_code` returns a tuple containing the file name and file path of the
    saved QR code image.
    """
    # Calculate the pixels per inch (dpi) based on a standard printer resolution
    dpi = 203

    # Calculate the size in pixels based on the desired size in centimeters and dpi
    size_inch = size_cm / 2.54
    size_pixels = int(size_inch * dpi)

    # Create the QR code with the specified size
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,  # This parameter is not used for size calculation
        border=4,
    )

    qr_data = resource.pid
    qr_data += ",\n" + qr_code_id

    if top_metadata:
        for metadata in top_metadata:
            qr_data += ",\n" + metadata

    qr.add_data(qr_data)
    qr.make(fit=True)

    # Determine the folder path based on the runtime context (bundled or development)
    if getattr(sys, 'frozen', False):
        # Bundled mode: Use sys.executable to get the bundled app's directory
        folder_path = os.path.join(os.path.dirname(sys.executable), 'qr_code')
    else:
        # Development mode: Use the script's directory as the base path
        script_dir = os.path.dirname(os.path.abspath(__file__))
        base_dir = os.path.dirname(script_dir)
        folder_path = os.path.join(base_dir, 'qr_code')

    # Create the qr_code folder if it doesn't exist
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    # Save the QR code image with the name qr_code_id.png and the specified size
    file_name = qr_code_id + ".png"
    file_path = os.path.join(folder_path, file_name)
    qr_image = qr.make_image(fill="black", back_color="white").resize((size_pixels, size_pixels))
    qr_image.save(file_path, dpi=(dpi, dpi))
    return file_name, file_path


def post_coscine(root, base_data, qr_code_id, size, display_name):
    """
    The function `post_coscine` takes in a root frame, base data, QR code ID, and size, and creates
    a resource in the Coscine project with the given metadata and uploads a QR code image to it.
    
    :param root: The main window or frame of the application. It is the
    starting point from which the project and resource paths are determined through the function create_resource()
    :param base_data: The `base_data` parameter is a dictionary that contains information about the base
    data for the QR code. It includes the following keys:
    :param qr_code_id: The `qr_code_id` parameter is the unique identifier for the QR code. It is used
    to create a resource in the `create_resource` function and to retrieve the resource in the
    `create_qr_code` function
    :param size: The "size" parameter in the "post_coscine" function is used to specify the size of the
    QR code image that will be created. It determines the dimensions of the QR code image in pixels
    :return: the file path of the uploaded QR code image and the QR code ID.
    """
    project, qr_code_id = create_resource(root, "https://purl.org/coscine/ap/base/", display_name)

    resource = project.resource(display_name)
    metadata = resource.metadata_form()
    metadata["Title"] = base_data["Title"]
    metadata["Creator"] = base_data["Creator"]
    metadata["Creation Date"] = datetime.now()
    metadata["Subject Area"] = base_data['Subject Area']
    metadata["Type"] = "Image"

    file_name, file_path = create_qr_code(resource, size, qr_code_id)

    # Upload the QR code image to Coscine
    resource.upload(file_name, file_path, metadata)
    download_metadata(qrcode_id=qr_code_id, metadata_form=metadata)

    return file_path, qr_code_id


def post_coscine_metadata(resource, qr_code_id, metadata, size):
    """
    The function `post_coscine_metadata` uploads a QR code image with associated metadata to a resource.
    
    :param resource: Resoure is a type of instance in Coscine, where all the information like the QR_Code png file
    and the metadata of your sample is stored.
    :param qr_code_id: The `qr_code_id` parameter is a unique identifier for the QR code. It is used to
    generate a unique file name and path for the QR code image
    :param metadata: The metadata parameter is a dictionary that contains additional information about
    the resource being uploaded. It can include key-value pairs such as the resource's title,
    description, author, creation date, etc
    :param size: The size parameter represents the size of the QR code image that will be generated. It
    determines the dimensions of the QR code, such as its width and height
    """
    file_name, file_path = create_qr_code(resource, size, qr_code_id)
    resource.upload(file_name, file_path, metadata)


def get_application_profiles():
    """
    The function `get_application_profiles` retrieves the names and URIs of application profiles from a client's
    vocabulary and returns them as a dictionary.
    :return: a dictionary with application profile names as keys and URIs as values.
    """
    client = initialize_coscine_client()
    app_profs = client.application_profiles()
    app_profiles = {item.name: item.uri for item in app_profs}
    return app_profiles


def get_metatadata_coscine(app_prof_name, app_prof_dict):
    """
    The function `get_metatadata_coscine` retrieves a list of metadata names from a specified
    application profile in the Coscine client.
    
    :param app_prof_name: The `app_prof_name` parameter is the name of the application profile for which
    you want to retrieve the metadata
    :return: a list of metadata names in English for a given application profile in the Coscine client.
    """
    client = initialize_coscine_client()
    uri = app_prof_dict.get(app_prof_name)
    app_prof = client.application_profile(uri)
    metadata_list = [item.name for item in app_prof.fields()]
    return metadata_list


def convert_to_string(data: List[Tuple[str, List[Union[bool, date, datetime, Decimal, int, float, str, time]]]]) -> str:
    result = []

    for item in data:
        key, values = item
        values_str = ', '.join(str(value) for value in values)
        result.append(f"{key}: [{values_str}]")

    return '[\n' + ',\n'.join(result) + '\n]'


def download_metadata(qrcode_id, metadata_form):
    """
    The function `download_metadata` downloads the metadata of a sample as a text file.
    
    :param qrcode_id: The `qrcode_id` parameter is the ID of the QR code associated with the sample. It
    is used to generate a unique filename for the downloaded metadata file
    :param metadata_form: The `metadata_form` parameter is an object that represents a form or a data
    structure containing the metadata information. It is assumed to have a method `as_str()` that
    returns the metadata as a string
    """
    confirm = messagebox.askyesno("Confirmation", "Download metadata of the sample?")

    if confirm:
        metadata_str = convert_to_string(metadata_form.items())
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%Y-%m-%d_%H-%M-%S")

        # Determine the base directory based on the runtime context (bundled or development)
        base_directory = os.path.dirname(sys.executable) if getattr(sys, 'frozen', False) else os.path.abspath(".")

        # Create a directory if it doesn't exist
        directory_name = os.path.join(base_directory, "Downloaded metadata")
        if not os.path.exists(directory_name):
            os.makedirs(directory_name)

        filename = os.path.join(directory_name, f"Metadata_{qrcode_id}_{formatted_datetime}.txt")

        with open(filename, "w") as file:
            file.write(metadata_str)

        messagebox.showinfo("Success", f"Metadata downloaded to {filename}")
