# SFB QR Code Generator

**Warning**:
_Right now, there is a problem with entries have options. Even though the user chooses Yes/No or any available options, Coscine API doesn't keep the information in this entry on its resource. User can still, however, download the complete inputted metadata in a form of txt file in the software.
Our recommendation is, save the metadata in the form of txt first and edit the empty entries in Coscine resources later accordingly._


Apologies for the inconvenience! As soon as we get a response from Coscine development team, we will fix this as soon as possible!


## Introduction

Welcome to the QR Code Generator software! This application allows you to create QR codes with associated metadata from Coscine application profile for various purposes. 

In this GUI, we use Coscine Python SDK (refer to the documentation here: https://coscine.pages.rwth-aachen.de/community-features/coscine-python-sdk/coscine.html).

This software is created as a part of FAIR principle of research data management in SFB1120. In order to achieve this, we use this software as a medium to send metadata of samples and their QR-Code to Coscine which is is a platform for your research data management that provides us PID for our data that are being kept there.

Follow the instructions below to get started.

## Installation

**Using set-up file (Recommended)**
Download the setup file QR_code_generator_WINDOWS_1.0.1_setup in the repository and follow the setup instructions then you are all set!

**Using the exe file (Recommended)**
Download the folder `dist` in the repository and run the exe file in the folder.

**Using the script**
1. Before running the application, make sure you have the required dependencies installed. These dependencies include the following 

Python libraries:

- customtkinter
- tkinter
- datetime
- os
- sys
- coscine
- cattr


2. Create a folder that will contain the script and the additional modules. You can choose any name you like but please make sure that all the modules and the script are in the same folder.


3. Download these additional modules and assets from the repository and please follow this instructions.

Additional modules (downloadable from this repository):

- api
- widget
- print

assets (downloadable from this repository):

- assets

This is how the file structure is supposed to look like

Root
|-- gui.py
|-- api
|-- widget
|-- print
|-- assets


## Troubleshooting
If you encounter any issues or errors during the process, check the console for error messages. Please contact incoming+warisa-roongaraya-sfb-qr-code-generator-92822-issue-@mail.git.rwth-aachen.de about the errors that you encountered. The progress of your issue can be tracked on Issue on the gitlab page.


## support
For additional support or inquiries, contact warisa.roongaraya@rwth-aachen.de.

Thank you for using the QR Code Generator software!
