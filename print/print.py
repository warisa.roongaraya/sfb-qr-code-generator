import os
import sys

import win32ui
from PIL import Image
from PIL import ImageWin

PRINTER_NAME = 'ZDesigner ZD421-203dpi ZPL'
PRINTER_DPI = 203


def cm_to_dots(cm, dpi):
    """
    The function `cm_to_dots` converts centimeters to dots based on the given DPI (dots per inch).
    
    :param cm: The length in centimeters that you want to convert to dots
    :param dpi: Dots per inch (dpi) is a measure of the resolution of a printer or display. It indicates
    the number of dots that can be placed in a line within one inch
    :return: the value of cm converted to dots based on the given dpi (dots per inch).
    """
    return int(cm * dpi / 2.54)


def print_square_image(qr_code_id, size_cm):
    """
    The function `print_square_image` prints a square image of a QR code with a specified size in
    centimeters.
    
    :param qr_code_id: The `qr_code_id` parameter is a string that represents the unique identifier of
    the QR code image. It is used to construct the file path of the QR code image file
    :param size_cm: The `size_cm` parameter represents the size of the square image in centimeters
    """
    # Determine the script_directory based on the runtime context (bundled or development)
    if getattr(sys, 'frozen', False):
        script_directory = os.path.dirname(sys.executable)
    else:
        print_dir = os.path.dirname(os.path.abspath(__file__))
        script_directory = os.path.dirname(print_dir)  # Go up one level

    qr_code_directory = os.path.join(script_directory, 'qr_code')
    image_path = os.path.join(qr_code_directory, qr_code_id + ".png")

    PHYSICALWIDTH = 110
    PHYSICALHEIGHT = 111
    hDC = win32ui.CreateDC()
    hDC.CreatePrinterDC(PRINTER_NAME)
    printer_size = hDC.GetDeviceCaps(PHYSICALWIDTH), hDC.GetDeviceCaps(PHYSICALHEIGHT)

    bmp = Image.open(image_path)
    if bmp.size[0] < bmp.size[1]:
        bmp = bmp.rotate(90)

    # Convert centimeters to dots for the square dimensions
    size_dots = cm_to_dots(size_cm, PRINTER_DPI)

    hDC.StartDoc(image_path)
    hDC.StartPage()

    dib = ImageWin.Dib(bmp)
    dib.draw(hDC.GetHandleOutput(), (50, 50, size_dots, size_dots))

    hDC.EndPage()
    hDC.EndDoc()
    hDC.DeleteDC()
