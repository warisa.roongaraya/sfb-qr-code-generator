import gettext
import os
import sys
import tkinter as tk
from datetime import datetime
from tkinter import *
from tkinter import ttk, messagebox, simpledialog

import customtkinter
from customtkinter import CTkEntry, CTkButton

from api import post_coscine, get_application_profiles, get_metatadata_coscine, create_resource
from print import print_square_image, PRINTER_NAME
from widget import ScrollableEntryFrame
from widget import get_token_file_path

customtkinter.set_appearance_mode("light")
customtkinter.set_default_color_theme("blue")

DOMAIN = 'qr_code_generator'
LOCALE_DIR = 'locales'

gettext.bindtextdomain(DOMAIN, LOCALE_DIR)
gettext.textdomain(DOMAIN)

lang = 'en'
_ = gettext.gettext


def change_language(new_lang):
    global lang
    lang = new_lang
    app.update_language()


def get_assets_directory():
    """
    The function `get_assets_directory()` returns the directory path for the assets folder, based on
    whether the code is running in bundled mode or development mode.
    :return: the directory path to the "frame0" folder within the "assets" folder.
    """
    if getattr(sys, 'frozen', False):
        # Running in bundled mode
        base_dir = os.path.dirname(sys.executable)
    else:
        # Running in development mode
        base_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(base_dir, "assets", "frame0")


def relative_to_assets(path: str) -> str:
    """
    The function `relative_to_assets` takes a path and returns the absolute path relative to the assets
    directory.
    
    :param path: A string representing the relative path to a file or directory
    :type path: str
    :return: a string that represents the path to a file relative to the assets directory.
    """
    assets_directory = get_assets_directory()
    return os.path.join(assets_directory, path)


class PageRecordToken(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.grid(row=0, column=0)
        self.create_widgets()

    def create_widgets(self):

        canvas_record_token = Canvas(
            self,
            bg="#FFFFFF",
            height=470,
            width=476,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )
        canvas_record_token.grid(row=0, column=0)
        token_entry_box = CTkEntry(canvas_record_token)
        canvas_record_token.create_image(238.0, 43, image=self.master.image_header)
        record_token_button = CTkButton(canvas_record_token, text="Record Token",
                                        command=lambda: self.save_token(token_entry_box))

        token_entry_box.place(x=170, y=120)
        record_token_button.place(x=170, y=200)
        self.check_existing_token()

    def save_token(self, token_entry_box):
        """
        The `save_token` function saves a token to a file named "api_token.txt" and displays a message box
        indicating that the token has been saved.
        """
        file_name = "api_token.txt"
        token = token_entry_box.get()
        if os.path.isfile(file_name):
            os.remove(file_name)
        with open(file_name, "w") as file:
            file.write(token)

        messagebox.showinfo("Token Saved", "Token has been saved.")
        self.master.show_page(self.master.page_generate_base)

    def check_existing_token(self):
        """
        The function checks if a token file already exists and prompts the user to change the token if it
        does.
        :return: a boolean value. If a token file already exists, the function will prompt the user with a
        messagebox asking if they want to change the token. If the user selects "yes", the function will
        return False. If the user selects "no" or if a token file does not exist, the function will return
        True.
        """
        token_file_path = get_token_file_path()

        if os.path.isfile(token_file_path):
            response = messagebox.askquestion("Change Token",
                                              "A token file already exists. Do you want to change the token?")
            if response == "no":
                self.master.show_page(self.master.page_generate_base)


class PageGenerateBase(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.canvas_generate_base = None
        self.entry_title = None
        self.entry_creator = None
        self.entry_time = None
        self.entry_sub_area = None
        self.grid(row=0, column=0)
        self.create_widgets()

    def create_widgets(self):
        self.canvas_generate_base = Canvas(
            self,
            bg="#FFFFFF",
            height=470,
            width=476,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )

        self.canvas_generate_base.grid(row=0, column=0)
        self.canvas_generate_base.create_image(238.0, 43, image=self.master.image_header)

        self.entry_title = CTkEntry(self.canvas_generate_base, 163, 20)
        self.entry_title.place(x=186.0, y=120.0)
        self.entry_creator = CTkEntry(self.canvas_generate_base, 163, 20)
        self.entry_creator.place(x=186.0, y=177.0)
        self.entry_time = CTkEntry(self.canvas_generate_base, 163, 20)
        self.entry_time.insert(0, datetime.now().strftime("%d/%m/%Y"))
        self.entry_time.place(x=186.0, y=237.0)
        self.entry_sub_area = CTkEntry(self.canvas_generate_base, 163, 20)
        self.entry_sub_area.insert(0, "Engineering Sciences")
        self.entry_sub_area.place(x=186.0, y=291.0)

        button_import_metadata = CTkButton(self.canvas_generate_base, 91, 37, text="Import metadata",
                                           fg_color="#858585", command=self.proceed_to_metadata_choice)
        button_import_metadata.place(x=205.0, y=345.0)
        button_generate_QR = CTkButton(self.canvas_generate_base, 91, 37, text="Generate QR Code",
                                       command=self.submit_data_base)
        button_generate_QR.place(x=200.0, y=400.0)

        self.canvas_generate_base.create_text(143.0, 123.0, anchor="nw", text="Title", fill="#70709B",
                                              font=("Roboto SemiBold", 14 * -1))
        self.canvas_generate_base.create_text(125.0, 180.0, anchor="nw", text="Creator", fill="#70709B",
                                              font=("Roboto SemiBold", 14 * -1))
        self.canvas_generate_base.create_text(86, 237, anchor="nw", text="Creation Date", fill="#70709B",
                                              font=("Roboto SemiBold", 14 * -1))
        self.canvas_generate_base.create_text(89, 294, anchor="nw", text="Subject Area", fill="#70709B",
                                              font=("Roboto SemiBold", 14 * -1))

    def record_base(self):
        """
        The function `record_base()` updates the values in the global `base_data` dictionary with the values
        entered in the `entry_1`, `entry_2`, and `entry_4` fields, and then clears the contents of those
        fields.
        """

        # Update the values in the global base_data dictionary
        self.master.base_data["Title"] = self.entry_title.get()
        self.master.base_data["Creator"] = self.entry_creator.get()
        self.master.base_data["Subject Area"] = self.entry_sub_area.get()
        # Clear Entry
        self.entry_title.delete(0, 'end')
        self.entry_creator.delete(0, 'end')
        self.entry_sub_area.delete(0, 'end')

    def submit_data_base(self):
        """
        The function `submit_data_base` generates a QR code, saves it as an image file, and prints a square
        image of the QR code using a specified printer.
        """

        display_name = simpledialog.askstring("Input Display Name", "Type in a display name for the resource:")
        if display_name is None or display_name.strip() == "":
            display_name = self.master.qr_code_id

        def generate_qr_code():
            self.record_base()
            size_cm = float(size_entry.get())
            qr_code_id = ""
            file_path, qr_code_id = post_coscine(self.canvas_generate_base, self.master.base_data, qr_code_id, size_cm,
                                                 display_name)  # Pass the size to the post_coscine function

            if file_path != None:
                messagebox.showinfo("Success", "Successfully created your sample!")
                try:
                    print_square_image(qr_code_id=qr_code_id, size_cm=float(size_cm))
                    messagebox.showinfo("Success",
                                        f"Printed a square image {size_cm} cm x {size_cm} cm to {PRINTER_NAME}.")
                except Exception as e:
                    messagebox.showerror("Error", f"Error printing to {PRINTER_NAME}: {str(e)}")
            else:
                messagebox.showerror("Error", "Failed to create a sample")
            qr_code_window.destroy()  # Close the QR code input window after generating the QR code

        qr_code_window = tk.Toplevel(self.master)
        qr_code_window.title("QR Code Size")

        qr_code_window.title("Insert the size of the QR Code")
        qr_code_window.geometry("200x100")
        qr_code_window.resizable(False, False)
        size_label = customtkinter.CTkLabel(qr_code_window, text="Enter QR Code Size (Centimeter):")
        size_label.pack()

        size_entry = customtkinter.CTkEntry(qr_code_window)
        size_entry.pack()

        generate_button = customtkinter.CTkButton(qr_code_window, text="Generate QR Code", command=generate_qr_code)
        generate_button.pack()

    def proceed_to_metadata_choice(self):
        """
        The function "proceed_to_metadata_choice" records the base, creates widgets for choosing a
        scheme, and shows the page for choosing a scheme.
        """

        self.master.scheme = get_application_profiles()
        self.master.page_choose_scheme.create_widgets()
        self.master.show_page(self.master.page_choose_scheme)


class PageChooseScheme(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.grid(row=0, column=0)

        self.metadata_scheme_combobox = None

    def create_widgets(self):
        canvas_choose_scheme = Canvas(
            self,
            bg="#FFFFFF",
            height=470,
            width=476,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )
        canvas_choose_scheme.grid(row=0, column=0)
        canvas_choose_scheme.create_image(238.0, 40, image=self.master.image_header)
        home_button = CTkButton(canvas_choose_scheme, 48, 48, text="Home", command=lambda: self.master.return_to_base())
        home_button.place(x=420, y=7)
        canvas_choose_scheme.create_text(128.5, 110.0, anchor="nw", text="Choose your metadata scheme", fill="#9E9E9E",
                                         font=("Roboto Bold", 16 * -1))
        metadata_scheme_variable = tk.StringVar(canvas_choose_scheme)
        self.metadata_scheme_combobox = ttk.Combobox(canvas_choose_scheme, textvariable=metadata_scheme_variable,
                                                     values=list(self.master.scheme.keys()), state="readonly")
        combobox_window = canvas_choose_scheme.create_window(174, 150, anchor="nw",
                                                             window=self.metadata_scheme_combobox)
        button_next = CTkButton(canvas_choose_scheme, 84, 48, text="Next", command=lambda: self.load_metadata(
            metadata_scheme_variable.get()))  #TBD Function of button4
        button_next.place(x=202, y=362)

    def update_combobox_values(self):
        """
        The function updates the values of a combobox with a new list of values.
        """
        self.metadata_scheme_combobox['values'] = list(self.master.scheme.keys())

    def load_metadata(self, chosen_metadata):
        """
        The function `load_metadata` loads metadata based on a chosen scheme, creates a resource with
        the selected scheme, and displays the widgets on page 3.
        
        :param chosen_metadata: The parameter `chosen_metadata` is the selected scheme for the metadata.
        It is the input chosen by the user to specify which metadata scheme to use
        """
        global metadata_list, entry_frame
        selected_scheme_name = chosen_metadata
        metadata_list = get_metatadata_coscine(selected_scheme_name, self.master.scheme)
        confirm = messagebox.askyesno("Confirmation", f"Create an object with scheme '{selected_scheme_name}'?")

        if confirm:
            display_name = simpledialog.askstring("Input Display Name", "Type in a display name for the object:")
            is_test_series = messagebox.askyesno("Confirmation", f"Are you creating a test series?")
            metadata_list = get_metatadata_coscine(selected_scheme_name, self.master.scheme)
            selected_scheme_uri = self.master.scheme.get(selected_scheme_name)
            self.master.project, self.master.qr_code_id = create_resource(self, selected_scheme_uri, display_name)
            if display_name is None or display_name.strip() == "":
                display_name = self.master.qr_code_id
            self.master.page_generate_scheme.create_widgets(display_name, is_test_series)
            self.master.show_page(self.master.page_generate_scheme)


class PageGenerateScheme(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.grid(row=0, column=0)
        self.entry_frame = None

    def create_widgets(self, display_name, is_test_series):
        canvas_generate_scheme = Canvas(
            self,
            bg="#FFFFFF",
            height=470,
            width=476,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )
        canvas_generate_scheme.grid(row=0, column=0)
        canvas_generate_scheme.create_image(238.0, 40, image=self.master.image_header)
        home_button = CTkButton(canvas_generate_scheme, 48, 48, text="Home",
                                command=lambda: self.master.return_to_base())
        home_button.place(x=420, y=7)
        #TODO: Pass is_test_series and make it work
        # Coscine needs to enable create_project within project.
        self.entry_frame = ScrollableEntryFrame(canvas_generate_scheme, width=300, height=200,
                                                project=self.master.project, qr_code_id=self.master.qr_code_id,
                                                display_name=display_name, is_test_series=is_test_series)
        self.entry_frame.place(x=94, y=100)
        button_generate_QR_w_mdata = CTkButton(canvas_generate_scheme, 84, 48, command=self.entry_frame.record_values,
                                               text="Generate QR Code")
        button_generate_QR_w_mdata.place(x=202, y=362)


class QRCodeGeneratorApp(tk.Tk):
    def __init__(self):
        super().__init__()

        self.image_header = tk.PhotoImage(file=relative_to_assets("header_image.png"))
        self.scheme = None
        self.base_data = {}
        self.metadata_list = []
        self.project = None
        self.qr_code_id = None
        self.test_series = False

        self.title("QR Code Generator")
        self.geometry("476x470")
        self.resizable(False, False)

        self.page_generate_scheme = PageGenerateScheme(self)
        self.page_choose_scheme = PageChooseScheme(self)
        self.page_generate_base = PageGenerateBase(self)
        self.page_record_token = PageRecordToken(
            self)  ##construct page_record_token later because it comes with a messagebox

        self.page_record_token.grid(row=0, column=0)
        self.page_generate_base.grid(row=0, column=0)
        self.page_choose_scheme.grid(row=0, column=0)
        self.page_generate_scheme.grid(row=0, column=0)

    def show_page(self, page):
        """
        The function "show_page" is used to display a specific page and bring it to the front.
        
        :param page: The "page" parameter is the name of the page that you want to show.
        """
        page.tkraise()

    def return_to_base(self):
        self.page_generate_base.tkraise()
        self.test_series = False


if __name__ == "__main__":
    app = QRCodeGeneratorApp()
    app.mainloop()
